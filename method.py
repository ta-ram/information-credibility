# -*- coding: utf-8 -*-
"""
Created on Sat Jul 13 18:56:20 2019

@author: Rama Aditya
"""
import pandas as pd
import numpy as np

def recall(tp,fn):
    calc = (tp/(tp+fn))*100
    return calc       

def precision(tp,fp):
    try:
        calc_prec = (tp/(tp+fp))*100
    except ZeroDivisionError:
        return 0
    return calc_prec

def akurasi(tp,tn,fn,fp):
    p = tp+fn
    n = fp+tn
    res = ((tp+tn)/(p+n))*100
    return res

def err_rate(tp,tn,fn,fp):
    p = tp+fn
    n = fp+tn
    hsl = ((fp+fn)/(p+n))*100
    return hsl

def fmeasure(prec,rec):
    fm = 2*((prec*rec)/(prec+rec))
    return fm

def gaussForm(pred,pv,stdev,stdevK,rata,rataK,probLabel):
    gauss,gaussKred = [],[]
    for i in range(len(pred)):
       e_pkt_tdk =  (-0.5 * pow((pred[i] - rata[i]),2)/(pow(stdev[i],2)))
       e_pkt_kred = (-0.5 * pow((pred[i] - rataK[i]),2)/(pow(stdevK[i],2)))
       gauss.append((1/(stdev[i]*np.sqrt(2*3.14))* pow(np.exp(1),e_pkt_tdk))*pv[i])
       gaussKred.append((1/(stdevK[i]*np.sqrt(2*3.14))*pow(np.exp(1),e_pkt_kred))*pv[i])
    
    gauss,gaussKred = np.array(gauss),np.array(gaussKred)
    p_fi_kred = gaussKred.prod()*probLabel[0]
    p_fi_tidak = gauss.prod()*probLabel[1]
    return p_fi_kred,p_fi_tidak