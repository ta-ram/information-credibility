# -*- coding: utf-8 -*-
"""
Created on Wed May  8 11:39:18 2019

@author: Rama Aditya
"""

import re
import csv
import nltk.classify

#start replaceTwoOrMore
def replaceTwoOrMore(s):
    #look for 2 or more repetitions of character
    pattern = re.compile(r"(.)\1{1,}", re.DOTALL) 
    return pattern.sub(r"\1\1", s)
#end

#proses tweet
def tweetProses(tweet):
    
    #konversi tweet ke lower case
    tweet = tweet.lower()
    #konversi teks link jadi teks URL
    tweet = re.sub('((www\.[^\s]+)|(https?://[^\s]+))','',tweet)
    #konversi username jadi teks AT_USER
    tweet = re.sub('@[^\s]+','',tweet)   
    #Hapus spasi tambahan
    tweet = re.sub('[\s]+', ' ', tweet)
    #Ganti hastag jadi kata
    tweet = re.sub(r'#([^\s]+)','', tweet)
    #trim
    tweet = tweet.strip('\'"')
    return tweet
#End of tweetProses

#ambil stopword file
def daftarSifatKerja(stopWordFile):
    #read stopword
    
    stopWord = []
#    stopWord.append('AT_USER')
#    stopWord.append('URL')
    
    file = open(stopWordFile,'r')
    baris = file.readline()
    while baris:
        word = baris.strip()
        stopWord.append(word)
        baris = file.readline()
    file.close()
    return stopWord
#End of daftarStopWord

#ambil kata kerja
def getverbWord(tweet,verbWord, adjWord,stopWord,verbAsingWord):
    featureVector = []
    verb = []
    negasi = ["tak","jangan","tidak","belum","ga","bukan"]
    words = tweet.split()
    for w in words:
        #replace two or more with two occurrences 
        w = replaceTwoOrMore(w) 
        #strip punctuation
        w = w.strip('\'"?,.')
        #check if it consists of only words
        val = re.search(r"^[a-zA-Z][a-zA-Z0-9]*[a-zA-Z]+[a-zA-Z0-9]*$", w)

        if(w in verbWord or val is None or w in negasi or w in adjWord or w in verbAsingWord):
            #cek kalo ada kata di daftar stopwordID
            if(w in stopWord or w.isdigit()):
                continue
            else:
                verb.append(w)
                #remove duplicate kata
                verb =  list(set(verb))
#        else:
#            featureVector.append(w.lower())
    
#    print(verb)
#    print("Jumlah Kata: ",len(verb)," kata")
    return verb

def removeStopWord(tweet,stopWord):
    featureVector = []
    words = tweet.split()
    
    for w in words:
        w = replaceTwoOrMore(w) 
        #strip punctuation
        w = w.strip('\'"?,.')
        #check if it consists of only words
        val = re.search(r"^[a-zA-Z][a-zA-Z0-9]*[a-zA-Z]+[a-zA-Z0-9]*$", w)
        #ignore if it is a stopWord
        if(w in stopWord or val is None):
            continue
        else:
            featureVector.append(w.lower())
    return featureVector   

def extract_features(tweet):
    tweet_words = set(tweet)
    features = {}
    for word in featureList:
        features['contains(%s)' % word] = (word in tweet_words)
#    print(features)
    return features 

#verbWord = daftarSifatKerja('kamus/verbID.txt')
#verbAsingWord = daftarSifatKerja('kamus/verbIDasing.txt')
posWord = daftarSifatKerja('kamus/positifWord.txt')
negWord = daftarSifatKerja('kamus/negatifWord.txt')
adjWord = daftarSifatKerja('kamus/adjID.txt')
stopWord = daftarSifatKerja('kamus/stopwordsID.txt')

#Load dataset
inputan = csv.reader(open('dataset/dataset99.csv', 'rt'), delimiter=',', quotechar='|')
featureList = []
tweets = []

for row in inputan:
    sentimen = row[0]
    tweet = row[1]
    prosesTweet = tweetProses(tweet)
    hpsStopWord = removeStopWord(prosesTweet,stopWord)
    #featureList = tweet yang sudah tidak ada stopword nya
    featureList.extend(hpsStopWord)
    tweets.append((hpsStopWord,sentimen))
#End Loop

featureList = list(set(featureList))

#Buat Training Set
dataTrain = nltk.classify.util.apply_features(extract_features,tweets)

#Start Training dengan Naive Bayes

NBClassifier = nltk.NaiveBayesClassifier.train(dataTrain)
#tweetTest = 'Presiden Jokowi tak pernah lelah untuk terus bekerja demi membangun Indonesia lebih baik. Salah satunya adalah pembangun rusun dan inilah potret ketika @jokowi meninjau salah satu ruangan rusun mahasiswa STKIP PGRI Tulungagung #SuaraJatimUntuk01 #01IndonesiaMaju #JokowiOrangBaik https://t.co/izJ3ECD1uO'
tweetTest = 'Bank Dunia Kritik Pedas Infrastruktur Jokowi Duh ngeri juga artikel ini, betapa semrawutnya pengelolaan terkait infrastruktur sejak 2015 lalu Mudah-mudahan setelah Ganti Presiden, akan diperbaiki oleh pimpinan yang baru #2019GantiPresiden  #17AprilPrabowoPresiden  https://t.co/o51122DQWU'
processedTweet = tweetProses(tweetTest)
sentimen = NBClassifier.classify(extract_features(removeStopWord(processedTweet,stopWord)))
featureVector = getverbWord(processedTweet, posWord, adjWord,stopWord,negWord)
print(sentimen,featureVector, "Jumlah Kata",sentimen," : ", len(featureVector))
#coba = tweetProses(tweetTest)
#print (featureVector)