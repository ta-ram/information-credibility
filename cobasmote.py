# -*- coding: utf-8 -*-
"""
Created on Sat Jul 13 20:49:58 2019

@author: USER
"""

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
from imblearn.over_sampling import SMOTE

df = pd.read_excel("dataset/data.xlsx")
df.drop(columns=['Tweet'], inplace=True)

y = df.label
X = df.drop('label', axis=1)

# setting up testing and training sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=27)

#sm = SMOTE(random_state=27, ratio=1.0)
#X_train, y_train = sm.fit_sample(X_train, y_train)
#
#smout = pd.DataFrame(X_train,y_train)
#print(smout)