# -*- coding: utf-8 -*-
"""
Created on Fri May 24 19:46:15 2019

@author: Rama Aditya
"""


import re
import csv
import nltk.classify

#start replaceTwoOrMore
def replaceTwoOrMore(s):
    #look for 2 or more repetitions of character
    pattern = re.compile(r"(.)\1{1,}", re.DOTALL) 
    return pattern.sub(r"\1\1", s)
#end

#proses tweet
def tweetProses(tweet):
    
    #konversi tweet ke lower case
    tweet = tweet.lower()
    #konversi teks link jadi teks URL
    tweet = re.sub('((www\.[^\s]+)|(https?://[^\s]+))','',tweet)
    #konversi username jadi teks AT_USER
    tweet = re.sub('@[^\s]+','',tweet)   
    #Hapus spasi tambahan
    tweet = re.sub('[\s]+', ' ', tweet)
    #Ganti hastag jadi kata
    tweet = re.sub(r'#([^\s]+)','', tweet)
    #trim
    tweet = tweet.strip('\'"')
    return tweet
#End of tweetProses

#ambil stopword file
def daftarSifatKerja(stopWordFile):
    #read stopword
    
    stopWord = []
#    stopWord.append('AT_USER')
#    stopWord.append('URL')
    
    file = open(stopWordFile,'r')
    baris = file.readline()
    while baris:
        word = baris.strip()
        stopWord.append(word)
        baris = file.readline()
    file.close()
    return stopWord
#End of daftarStopWord

#ambil kata kerja
#def getverbWord(tweet,verbWord, adjWord,stopWord,verbAsingWord,adjAsingWord,noise):
#    featureVector = []
#    verb = []
#    negasi = ["tak","jangan","tidak","belum","ga","gak","bukan","nggak","kaga", "tiada"]
#    words = tweet.split()
##    print(words)
#    for w in words:
#        #replace two or more with two occurrences 
#        w = replaceTwoOrMore(w) 
#        #strip tanda baca 
#        w = w.strip('\'"?),.!-:|(')
#        #check if it consists of only words
#        val = re.search(r"^[a-zA-Z][a-zA-Z0-9]*[a-zA-Z]+[a-zA-Z0-9]*$", w)
#
#        if(w in verbWord or val is None or w in negasi or w in adjWord or w in verbAsingWord or w in adjAsingWord):
#            #cek kalo ada kata di daftar stopwordID
#            if(w in stopWord or w.isdigit() or w in noise or w is ''):
#                continue
#            else:
#                verb.append(w)
#                #remove duplicate kata
#                verb =  list(set(verb))
##        else:
##            featureVector.append(w.lower())
#    
##    print(verb)
##    print("Jumlah Kata: ",len(verb)," kata")
#    return verb

#def splitTextonWords(Text, numberOfWords=1):
def pattern(text,words,kataPos,kataNeg):
# =============================================================================
#         Untuk 1 kata positif/negatif        
#    pattern = '(ada\s\w*)'
# =============================================================================
    print("------> ",text)
    if(re.search("salahnya",text)):
        kt = re.findall("salahnya",text)
        kataNeg.append(kt[0])
    if(re.search("salah\s\w*",text)):
        stopWords = re.findall("salah\s\w*",text)
        for idx, s in enumerate(stopWords):
            kt = stopWords[idx]
            # =============================================================================
            # split kata stopword 2 kata biar ga masuk salah satunya ke list. 
            # contoh 'salah satu' => 
            # salah nya masuk gara2 ada di negword
            # =============================================================================
            q = stopWords[idx].split()
            for idQ, w in enumerate(q):
#                print(q[idQ])
                p = words.remove(q[idQ])
            if(kt in stopWord):
               print("stopWord => ",kt)
               continue
            if p != None:
                kataNeg.append(p)
            if(kt in negWord):
                kataNeg.append(kt)
            elif(kt in posWord):
                kataPos.append(kt)
#    else: #else karena search kata salah blabla ga ketemu
        for w in words:
            if(w in posWord):
                kataPos.append(w)
            elif(w in negWord):
                kataNeg.append(w)
#        return text
#    
#def patternNegasi(text,words,kataPos,kataNeg):
    patternAda = "(?:belum|tidak|tak|ga|g|gak|kaga|jangan)\sada\s\w*"
    patternTidak = "(?:belum|tidak|tak|ga|g|gak|kaga|jangan)\s\w*"
    patternTidak3 = "(?:belum|tidak|tak|ga|g|gak|kaga|jangan)\s\w*\s\w*"
    idx_kata = 0
    while(idx_kata < len(words)):
        
        kt = words[idx_kata]
# =============================================================================
#         Untuk negasi terdiri dari 2 kata mis: jangan ada blabla
# =============================================================================
        
        if re.search(patternAda,text):
            print(re.search(patternAda,text))
            x = re.findall(patternAda,text)
            kt = x[idx_kata]
            print(kt)
            if(kt in posWord):
#                print(kt)
                kataPos.append(kt)
            if(kt in negWord):
#                print("Kata Negatif => ",kt)
                kataNeg.append(kt)
            for w in words:
                if(w in posWord):
                    kataPos.append(w)
                elif(w in negWord):
                    kataNeg.append(w)
                
        elif re.search(patternTidak,text):
#            print(re.search(patternTidak,text))
            words = re.findall(patternTidak,text)
            kt = words[idx_kata]
            if(kt in posWord):
#                print(kt)
                kataPos.append(kt)
            if(kt in negWord):
#                print("Kata Negatif => ",kt)
                kataNeg.append(kt)
                
        elif re.search("(?:bukan)\s\w*",text):
            words = re.findall("(?:bukan)\s\w*",text)
            kt = words[idx_kata]
            if(kt in posWord):
#                print(kt)
                kataPos.append(kt)
            if(kt in negWord):
#                print("Kata Negatif => ",kt)
                kataNeg.append(kt)
# =============================================================================
#         Untuk negasi terdiri dari 3 kata mis: tak pernah lelah
# =============================================================================
        if re.search(patternTidak3,text):
            #ex: tak pernah lelah
            words = re.findall(patternTidak3,text)
            kt = words[idx_kata]
            if(kt in posWord):
                kataPos.append(kt)
            if(kt in negWord):
#                print("Kata Negatif => ",kt)
#                print(kt)
                kataNeg.append(kt)
        idx_kata+=1
        return text
def getPosNeg(tweet,posWord, negWord,stopWord):
    kataPos = []
    kataNeg = []
    negasi = ["tak","jangan","tidak","belum","ga","gak","bukan","nggak","kaga", "tiada","anti"]
    text = re.sub(r'[^\w\s]','',tweet)
    words = text.split()
#    for p in words:
#        if(p in negWord):
    pattern(text,words,kataPos,kataNeg)
#        elif(p in posWord):
##            kataPos.append(p)
#            pattern(text,words,kataPos,kataNeg)
#   cek kata negasi yg punya pattern
#    patternNegasi(text,words,kataPos,kataNeg)
    
    
#    print(text)
    print("=================================================")
    print("Jumlah kata Positif: ",len(list(set(kataPos))), "=>", list(set(kataPos)) )
    print("Jumlah kata Negatif: ",len(list(set(kataNeg))),"=>", list(set(kataNeg)))
    print("=================================================")
    return kataPos,kataNeg


def removeStopWord(tweet,stopWord):
    featureVector = []
    words = tweet.split()
    
    for w in words:
        w = replaceTwoOrMore(w) 
        #strip punctuation
        w = w.strip('\'"?,.)(')
        #check if it consists of only words
        val = re.search(r"^[a-zA-Z][a-zA-Z0-9]*[a-zA-Z]+[a-zA-Z0-9]*$", w)
        #ignore if it is a stopWord
        if(w in stopWord or val is None):
            continue
        else:
            featureVector.append(w.lower())
    return featureVector   

def replaceAlay(word,kbba):
    listAlay = {}
    global rep
    for i in kbba:
        listAlay[i[0]] = i[1]
        
    for x in word.split():
#        idx =  word.split().index(x)
        if(x in listAlay):
#            print(idx, x,listAlay[x])
            word = word.replace(x,listAlay[x])
    print(word)
            
#verbWord = daftarSifatKerja('kamus/verbID.txt')
#verbAsingWord = daftarSifatKerja('kamus/verbIDasing.txt')
posWord = daftarSifatKerja('kamus/positifWord.txt')
negWord = daftarSifatKerja('kamus/negatifWord.txt')
#adjWord = daftarSifatKerja('kamus/adjID.txt')
stopWord = daftarSifatKerja('kamus/stopwordsID.txt')
#adjAsingWord = daftarSifatKerja('kamus/adjIDasing.txt')
#noise = daftarSifatKerja('kamus/noise_untuk_pelabelan_dataset.txt')
#kbba = daftarSifatKerja('kamus/kbba.txt')

#tweetTest = 'DEFINISI NORAK! GA MAJU! DAN TIDAK BERKEMBANG 02 ITU! ga ada ga bayar ada hasilnya Tapi berkembang, tiada ragu, tidak ada, tidak bilang, tidak main-main bertambah kedunguan #DebatTerakhirPilpres2019 #DebatPilpres2019 #DebatKelimaPilpres2019 #OptimisIndonesiaMaju #JokowiMenangTotalDebat https://t.co/KREhZC5Xnr'
#tweetTest = 'Gmn makmurkan rakyat indonesia, karyawan sendiri di beri harapan palsu @projopusat @CH_chotimah @2019tetapjkwma @Dennysiregar7 @enylove2 @NannyAlexendry @haikal_hassan @habibthink #JokowiLagi #JKWMerahPutih #KoalisiPraBohong #01IndonesiaMaju #IndonesiaBarokah #MyDaySelcaDay #malamminggu https://t.co/eBHyvq7EzP ' 
#Load data set yang belum dilabeln
inputan = csv.reader(open('dataset/calcnegword.csv', 'rt'), delimiter=';')
kbba = csv.reader(open('kamus/kbba.csv', 'rt'), delimiter=';')
#replaceAlay(kbba)
tweets = []
count = 0

for row in inputan:
    processedTweet = tweetProses(row[0])
    featureVector = getPosNeg(processedTweet, posWord,negWord,stopWord)
##        rep = replaceAlay(row[0],kbba)
    count+=1
#    print (featureVector)
#processedTweet = tweetProses(tweetTest)
#featureVector = getPosNeg(processedTweet, posWord,negWord,stopWord)