# -*- coding: utf-8 -*-
"""
Created on Sun Jun 30 09:24:09 2019

@author: Rama Aditya
"""
import pandas as pd
import numpy as np
from statistics import mean
from method import *
import math

matrix = pd.read_excel("matrixpairwise.xlsx", index_col = 0)
pv = pd.read_excel("pve.xlsx")
pv = pv.values.T.tolist()

data = pd.read_excel("dataset/data188.xlsx")
#data = pd.read_excel("dataset/newdata_upsample.xlsx")

probLabel = np.zeros(2)
rata, rataK,stdev,stdevK = np.zeros(6),np.zeros(6),np.zeros(6),np.zeros(6)

data.drop(columns=['Tweet'], inplace=True)

data_random = data.sample(frac=1,random_state = 2)
kFold = 9
data.head()

data_train, data_test,predict_lbl = [],[], []


for d in range(len(data)):
    tdkred = data[data['label'] == 'Tidak Kredibel']
    krd = data[data['label'] == 'Kredibel']

panjang_calc = len(data_random) / kFold
panjang = math.ceil(panjang_calc)

awal = 0
for i in range(kFold):
    copy_data = data_random.copy()
    data_test.append(data_random[awal:awal+panjang])
    copy_data = copy_data.drop(copy_data.index[awal:awal+panjang])
    data_train.append(copy_data)
    awal = awal+panjang

list_akurasi, list_confusion,tp_sum,tn_sum,fp_sum,fn_sum,conf_sum = [],[], [],[],[],[],[]
list_tp,list_tn,list_fp,list_fn = [],[],[],[]
arr_pred,arr_act = [],[]

for fold in range(kFold):
    
    train, test = data_train[fold], data_test[fold]
    tdk_kred = train[train['label'] == 'Tidak Kredibel']
    kred = train[train['label'] == 'Kredibel']
    
    rata[0] = mean(tdk_kred['favorit'])
    rata[1] = mean(tdk_kred['kata'])
    rata[2] = mean(tdk_kred['karakter'])
    rata[3] = mean(tdk_kred['rt'])
    rata[4] = mean(tdk_kred['kata_pos'])
    rata[5] = mean(tdk_kred['kata_neg'])
    
    stdev[0] = np.std(tdk_kred['favorit'])
    stdev[1] = np.std(tdk_kred['kata'])
    stdev[2] = np.std(tdk_kred['karakter'])
    stdev[3] = np.std(tdk_kred['rt'])
    stdev[4] = np.std(tdk_kred['kata_pos'])
    stdev[5] = np.std(tdk_kred['kata_neg'])
    
    rataK[0] = mean(kred['favorit'])
    rataK[1] = mean(kred['kata'])
    rataK[2] = mean(kred['karakter'])
    rataK[3] = mean(kred['rt'])
    rataK[4] = mean(kred['kata_pos'])
    rataK[5] = mean(kred['kata_neg'])
    
    stdevK[0] = np.std(kred['favorit'])
    stdevK[1] = np.std(kred['kata'])
    stdevK[2] = np.std(kred['karakter'])
    stdevK[3] = np.std(kred['rt'])
    stdevK[4] = np.std(kred['kata_pos'])
    stdevK[5] = np.std(kred['kata_neg'])
    
    label = train['label'].tolist() #y_train
    data = train.values.tolist()
    X_train = train.iloc[:,0:6].values
    
    
    for i in range(len(label)):
        if (label[i] == 'Kredibel'):
            probLabel[0] += 1
        else:
            probLabel[1] += 1
    
    probLabel = probLabel / np.sum(probLabel)
    
    label_aktual = test["label"].values
    test.drop(columns=['label'], inplace=True)
    
    pred = test.iloc[:,0:6].values
    pv = [0.371848,0.241382,0.182809,0.102705,0.050628,0.050628]

    predict = [gaussForm(pred[index],pv,stdev,stdevK,rata,rataK,probLabel) for index in range(len(pred))]
    
    #Pengelompokkan 
    for index in range(len(predict)):
        if(predict[index][0] > predict[index][1]):
            predict_lbl.append("Kredibel")
        else:
            predict_lbl.append("Tidak Kredibel")
            
    #Confusion Matrix
    tp, tn, fp, fn = 0,0,0,0

    benar = 0
    for w in range(len(test)):
        obj_pred = predict_lbl[w]
        obj_act = label_aktual[w]
        
        arr_pred.append(obj_pred)
        arr_act.append(obj_act)
        
        if (obj_act == "Kredibel"):
            if (obj_pred == "Kredibel"):
                list_tp.append([w,label_aktual[w],predict_lbl[w]])
                tp += 1
            else:
                list_fn.append([w,label_aktual[w],predict_lbl[w]])
                fn += 1
        else:
            if (obj_pred == "Kredibel"):
                list_fp.append([w,label_aktual[w],predict_lbl[w]])
                fp += 1
            else:
                list_tn.append([w,label_aktual[w],predict_lbl[w]])
                tn += 1
        
        if(obj_pred == obj_act):    
            benar+=1
            
    list_akurasi.append(benar/len(test))
    list_confusion.append([tp,tn,fp,fn])
    
#    prec = precision(list_confusion[fold][0],list_confusion[fold][2])
#    rec = recall(list_confusion[fold][0],list_confusion[fold][3])
#    print(fold+1,"Precision:%s, Recall:%s" % (round(prec,2),round(rec,2)))
    
    tp_sum.append(list_confusion[fold][0])
    tn_sum.append(list_confusion[fold][1])
    fp_sum.append(list_confusion[fold][2])
    fn_sum.append(list_confusion[fold][3])
    
conf_sum.append([sum(tp_sum),sum(tn_sum),sum(fp_sum),sum(fn_sum)])
err_cm = err_rate(conf_sum[0][0],conf_sum[0][1],conf_sum[0][2],conf_sum[0][3])
prec_cm = precision(conf_sum[0][0],conf_sum[0][2])
rec_cm = recall(conf_sum[0][0],conf_sum[0][3])
f1 = fmeasure(prec_cm,rec_cm)
print("TP: %s, TN: %s, FP: %s, FN: %s" % (conf_sum[0][0],conf_sum[0][1],conf_sum[0][2],conf_sum[0][3]))
print(round(prec_cm,2),round(rec_cm,2),round(f1,2))

#dict = {'data_aktual':arr_act,'prediksi':arr_pred}
#df = pd.DataFrame(dict, columns= ['data_aktual','prediksi'])
#df.to_excel('asd.xlsx',index=None,header=True)