# -*- coding: utf-8 -*-
"""
Created on Wed May  8 11:39:18 2019

@author: Rama Aditya
"""

import re
import csv
import nltk.classify

#start replaceTwoOrMore
def replaceTwoOrMore(s):
    #look for 2 or more repetitions of character
    pattern = re.compile(r"(.)\1{1,}", re.DOTALL) 
    return pattern.sub(r"\1\1", s)
#end

#proses tweet
def tweetProses(tweet):
    
    #konversi tweet ke lower case
    tweet = tweet.lower()
    #konversi teks link jadi teks URL
    tweet = re.sub('((www\.[^\s]+)|(https?://[^\s]+))','',tweet)
    #konversi username jadi teks AT_USER
    tweet = re.sub('@[^\s]+','',tweet)   
    #Hapus spasi tambahan
    tweet = re.sub('[\s]+', ' ', tweet)
    #Ganti hastag jadi kata
    tweet = re.sub(r'#([^\s]+)','', tweet)
    #trim
    tweet = tweet.strip('\'"')
    return tweet
#End of tweetProses

#ambil stopword file
def daftarSifatKerja(stopWordFile):
    #read stopword
    
    stopWord = []
#    stopWord.append('AT_USER')
#    stopWord.append('URL')
    
    file = open(stopWordFile,'r')
    baris = file.readline()
    while baris:
        word = baris.strip()
        stopWord.append(word)
        baris = file.readline()
    file.close()
    return stopWord
#End of daftarStopWord

#ambil kata kerja
def getverbWord(tweet,verbWord, adjWord,stopWord,verbAsingWord,adjAsingWord,noise):
    featureVector = []
    verb = []
    negasi = ["tak","jangan","tidak","belum","ga","gak","bukan","nggak"]
    words = tweet.split()
#    print(words)
    for w in words:
        #replace two or more with two occurrences 
        w = replaceTwoOrMore(w) 
        #strip tanda baca 
        w = w.strip('\'"?),.!-:|(')
        #check if it consists of only words
        val = re.search(r"^[a-zA-Z][a-zA-Z0-9]*[a-zA-Z]+[a-zA-Z0-9]*$", w)

        if(w in verbWord or val is None or w in negasi or w in adjWord or w in verbAsingWord or w in adjAsingWord):
            #cek kalo ada kata di daftar stopwordID
            if(w in stopWord or w.isdigit() or w in noise or w is ''):
                continue
            else:
                verb.append(w)
                #remove duplicate kata
                verb =  list(set(verb))
#        else:
#            featureVector.append(w.lower())
    
#    print(verb)
#    print("Jumlah Kata: ",len(verb)," kata")
    return verb

def removeStopWord(tweet,stopWord):
    featureVector = []
    words = tweet.split()
    
    for w in words:
        w = replaceTwoOrMore(w) 
        #strip punctuation
        w = w.strip('\'"?,.)(')
        #check if it consists of only words
        val = re.search(r"^[a-zA-Z][a-zA-Z0-9]*[a-zA-Z]+[a-zA-Z0-9]*$", w)
        #ignore if it is a stopWord
        if(w in stopWord or val is None):
            continue
        else:
            featureVector.append(w.lower())
    return featureVector   

def replaceAlay(word,kbba):
    listAlay = {}
    global rep
    for i in kbba:
        listAlay[i[0]] = i[1]
        
    for x in word.split():
#        idx =  word.split().index(x)
        if(x in listAlay):
#            print(idx, x,listAlay[x])
            word = word.replace(x,listAlay[x])
    print(word)
            
verbWord = daftarSifatKerja('kamus/verbID.txt')
verbAsingWord = daftarSifatKerja('kamus/verbIDasing.txt')
#posWord = daftarSifatKerja('kamus/positifWord.txt')
#negWord = daftarSifatKerja('kamus/negatifWord.txt')
adjWord = daftarSifatKerja('kamus/adjID.txt')
stopWord = daftarSifatKerja('kamus/stopwordsID.txt')
adjAsingWord = daftarSifatKerja('kamus/adjIDasing.txt')
noise = daftarSifatKerja('kamus/noise_untuk_pelabelan_dataset.txt')
#kbba = daftarSifatKerja('kamus/kbba.txt')

#tweetTest = 'Bentuk dukungan timbal balik antara Presiden dengan rakyat. Presiden ikut bantu promosikan usaha dengan membuat vlog, masyarakat yang puas akan kinerja nyata Jokowi pun memberikan hadiah #SuaraJatimUntuk01 #01IndonesiaMaju #JokowiOrangBaik #JelasIslamnya'
#Load data set yang belum dilabeln
inputan = csv.reader(open('dataset/dataset500an.csv', 'rt'), delimiter=';')
kbba = csv.reader(open('kamus/kbba.csv', 'rt'), delimiter=';')
#replaceAlay(kbba)
tweets = []
count = 0

for row in inputan:
    processedTweet = tweetProses(row[0])
#    rep = replaceAlay(row[0],kbba)
    featureVector = getverbWord(processedTweet, verbWord, adjWord,stopWord,verbAsingWord,adjAsingWord, noise)
    count+=1
    print (featureVector)
#processedTweet = tweetProses(tweetTest)
#featureVector = getverbWord(processedTweet, verbWord, adjWord,stopWord,verbAsingWord)
