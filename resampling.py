# -*- coding: utf-8 -*-
"""
Created on Sat Jul 13 19:06:35 2019

@author: Rama Aditya
"""
from sklearn.utils import resample
import pandas as pd
import numpy as np
from imblearn.over_sampling import SMOTE


#Load Data
data = pd.read_excel("dataset/data.xlsx")

#data.drop(columns=['Tweet'], inplace=True)

data_random = data.sample(frac=1,random_state=2)
data.head()

def oversampling(data):
    for d in range(len(data)):
        tdkred = data[data['label'] == 'Tidak Kredibel']
        krd = data[data['label'] == 'Kredibel']
        
    kred_upsample = resample(krd, replace=True, n_samples=len(tdkred))
    upsample = pd.concat([tdkred, kred_upsample])
    return upsample


def undersampling(data):
    for d in range(len(data)):
        tdkred = data[data['label'] == 'Tidak Kredibel']
        krd = data[data['label'] == 'Kredibel']
    tdkrd_downsample = resample(tdkred, replace=False, n_samples=len(krd))
    downsample = pd.concat([tdkrd_downsample, krd])
    return downsample

def smote(data):
    X_train = data[['favorit','kata','karakter','rt','kata_pos','kata_neg']]
    y_train = data[['label']].values.ravel()
    sm = SMOTE(random_state=27, ratio=1.0)
    X_train_, y_train_ = sm.fit_resample(X_train, y_train)
    df_x = pd.DataFrame(X_train_, columns=['favorit','kata','karakter','rt','kata_pos','kata_neg'])
    df_y = pd.DataFrame(y_train_, columns=['label'])
    df_x['label'] = df_y
    return df_x


upsample = oversampling(data)
downsample = undersampling(data)    
upsmote = smote(data)

upsmote.to_excel('dataset/run_smote.xlsx',index=None,header=True)

#df = pd.DataFrame(upsample)
#df.to_excel('dataset/run_upsample.xlsx',index=None,header=True)
#
#df1 = pd.DataFrame(downsample)
#df1.to_excel('dataset/run_downsample.xlsx',index=None,header=True)
