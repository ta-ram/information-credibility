# -*- coding: utf-8 -*-
import csv
import re
import nltk
import numpy as np
import pandas as pd
import itertools
from sys import exit
from collections import OrderedDict 

"""
Created on Thu May 30 22:21:37 2019

@author: Rama Aditya
"""
def tweetProses(tweet):
    #konversi tweet ke lower case
    tweet = tweet.lower()
    #konversi teks link jadi teks URL
    tweet = re.sub('((www\.[^\s]+)|(https?://[^\s]+))','',tweet)
    #konversi username jadi teks AT_USER
    tweet = re.sub('@[^\s]+','',tweet)   
    #Hapus spasi tambahan
    tweet = re.sub('[\s]+', ' ', tweet)
    #Ganti hastag jadi kata
    tweet = re.sub(r'#([^\s]+)','', tweet)
    
    #trim
    tweet = tweet.strip('\'"')
    
    punctuations = '''!()-[]{};:'"\,<>./?@#$%^&*_~???'''
    no_punct = ""

    no_punct = re.sub('[^\w\s]', ' ', tweet)
    return no_punct

def bukaFile(filenya):
    
    kata = []
    
    file = open(filenya,'rt')
    baris = file.readline()
    while baris:
        word = baris.strip()
        kata.append(word)
        baris = file.readline()
    file.close()
    return kata

def stopword(tweet,stopWord):
    global filtered
    words = tweet.split()
    words = list(dict.fromkeys(words))
    filtered = [w for w in words if not w in stopWord]    
    
    filtered = []
    for w in words:
        if (w not in stopWord and not w.isdigit()):
           filtered.append(w) 
    return filtered

def intersection(lst1, lst2): 
    lst3 = [value for value in lst1 if value in lst2] 
    return lst3 

def combin(remWord):
    temp = 0
    global ktPos,ktNeg
    count = 0
    s = []
    gab = []
    ktPos = [p for p in remWord if not p in posWord]
    ktNeg = [n for n in remWord if not n in negWord]
    for i in range(0,len(remWord)):
        count+=1    
        for w in itertools.combinations(remWord,count):
             temp+=1
             x = ' '.join(w)
             if(x in posWord):
                 ktPos.append(x)
             elif(x in negWord):
                 ktNeg.append(x)
                 
    gab = ktPos+ktNeg
    for k in range(0,len(gab)):
        for y in range(0,len(gab)):
            if(gab[k] in gab[y]): 
                if(len(gab[y]) > len(gab[k])):
                    if not any(gab[k] in m for m in s):
                        s.append(gab[y])
            else:
                if not any(gab[k] in m for m in s):
                    s.append(gab[k])
    ktPos = []
    ktNeg = []
    for q in s:
      if(q in posWord):
        ktPos.append(q)
      elif(q in negWord):
        ktNeg.append(q)  

    return ktPos,ktNeg

posWord = bukaFile('kamus/positifWord.txt')
negWord = bukaFile('kamus/negatifWord.txt')
stopWord = bukaFile('kamus/stopwordsID.txt')
#psn = 'Nah, ketika masuk ke dalam kedai kopi, Presiden disuguhi dengan hiburan nyanyian dua lagu yang bergenre rap oleh para anak muda Tulungagung #SuaraJatimUntuk01 #01IndonesiaMaju #JokowiOrangBaik #JelasIslamnya #JokowiHarapanPasti'
#t = tweetProses(psn)
#print(t)
#words = t.split()
idx = 0
negatif = 0
positif = 0
gab = []
tweets = []
nilaiP = []
nilaiN = []
fav = []
kt = []
ch = []
rt = []
count = 0
temp = 0
ktPos = []
ktNeg = []

testing = 'dataset/testing/cekata.csv'
asli = 'dataset/dataCrawl.csv'

file = pd.read_csv(asli,skiprows=0,sep=';',encoding='latin1')
inputan = np.array(file)

jkt = []
no = 0

for itm in range(len(inputan)):
    no+=1
    
    row = inputan[itm][0]
    processedTweet = tweetProses(row)
    remWord = stopword(processedTweet,stopWord)
    print("\n")
    print("Data ke- ",no)
    
    if(len(remWord) == 1):
        
        tweets.append(processedTweet)
        fav.append(inputan[itm][1])
        kt.append(len(processedTweet.split()))
        ch.append(len(processedTweet))
        rt.append(inputan[itm][2])
        jkt.append(len(remWord))
        
        
        if(remWord[0] in posWord):
            ktPos.append(remWord[0])
            
        elif(remWord[0] in negWord):
            ktNeg.append(remWord[0])
        
        nilaiN.append(len(ktNeg))
        nilaiP.append(len(ktPos))
        
        print("\n")             
        print("===Positif===> ",len(ktPos))
        print("===Negatif===> ",len(ktNeg))
        
        
    else:
        kata = combin(remWord)  
        nilaiP.append(len(kata[0]))
        nilaiN.append(len(kata[1]))
        
        tweets.append(processedTweet)
        fav.append(inputan[itm][1])
        kt.append(len(processedTweet.split()))
        ch.append(len(processedTweet))
        rt.append(inputan[itm][2])
        jkt.append(len(remWord))
        
        print("\n")             
        print("===Positif===> ",len(kata[0]))
        print("===Negatif===> ",len(kata[1]))
        
#    dict = {'tweets': tweets, 'fav':fav,'kata':kt,'karakter':ch,'rt':rt,'positif': nilaiP,'negatif':nilaiN,'kata_diproses':jkt}
#    df = pd.DataFrame(dict) 
#    df.to_csv('dataset/res_dataCrawl.csv',sep=';',index=False)
    

        
    ktPos = []
    ktNeg = []

